﻿using System.Collections.Generic;
using System.Globalization;
using Localization.CoreLibrary.Pluralization;
using Microsoft.Extensions.Localization;

namespace JsonDictionaryLoader
{
    public interface ILocalizationDictionary
    {
        /// <summary>
        /// Loads dictionary from file.
        /// </summary>
        /// <param name="filePath">Resource file path.</param>
        /// <returns>This instance.</returns>
        ILocalizationDictionary Load(string filePath);
        /// <summary>
        /// Dictionary culture info.
        /// </summary>
        /// <returns>Dictionary culture info.</returns>
        CultureInfo CultureInfo();
        /// <summary>
        /// Dictionary scope.
        /// </summary>
        /// <returns>Dictionary scope.</returns>
        string Scope();
        /// <summary>
        /// Dictionary resource file extension. e.g "json", "xml".
        /// </summary>
        /// <returns>Resource file extension.</returns>
        string Extension();
        /// <summary>
        /// Dictionary.
        /// </summary>
        /// <returns>All key-value strings.</returns>
        Dictionary<string, LocalizedString> List();
        /// <summary>
        /// Dictionary.
        /// </summary>
        /// <returns>All pluralized strings.</returns>
        Dictionary<string, PluralizedString> ListPlurals();
        /// <summary>
        /// Dictionary
        /// </summary>
        /// <returns>All special constant strings.</returns>
        Dictionary<string, LocalizedString> ListConstants();
    }
}