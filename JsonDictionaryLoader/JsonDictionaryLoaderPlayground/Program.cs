﻿using System;
using System.Collections.Generic;
using JsonDictionaryLoader;
using Localization.CoreLibrary.Pluralization;
using Microsoft.Extensions.Localization;

namespace JsonDictionaryLoaderPlayground
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            ILocalizationDictionary ld = new JsonLocalizationDictionary();
            ld.Load("C:\\Pool\\JsonDictionaryLoader\\JsonDictionaryLoaderPlayground\\cs-CZ.json");

            Console.Out.WriteLine("######################################################");
            Console.Out.WriteLine("Culture: " + ld.CultureInfo());
            Console.Out.WriteLine("Scope: " + ld.Scope());
            Console.Out.WriteLine("######################################################");

            Dictionary<string, LocalizedString> dictionary = ld.List();
            Dictionary<string, LocalizedString> constantsDictionary = ld.ListConstants();
            Dictionary<string, PluralizedString> pluralsDictionary = ld.ListPlurals();

            foreach (KeyValuePair<string, LocalizedString> localizedString in dictionary)
            {
                Console.Out.WriteLine(localizedString.Key + " := " + localizedString.Value);
            }

            Console.In.ReadLine();
        }
    }
}
